#
# Tox global configuration
#
[tox]
envlist = py{35,36,37}
skip_missing_interpreters = true
isolated_build = true

#
# Defaults for all environments
#
# Anything specified here is inherited by the sections
#
[testenv]
usedevelop =
    # This is required by Cython in order to get coverage for cython files.
    py{35,36,37}-!nocover: True

commands =
    # Running with coverage reporting enabled
    py{35,36,37}-!external-!nocover: pytest --basetemp {envtmpdir} --cov=buildstream --cov-config .coveragerc {posargs}
    py{35,36,37}-!nocover: mkdir -p .coverage-reports
    external-!nocover: {envpython} {toxinidir}/tests/external_plugins.py {envtmpdir} --cov=buildstream --cov-append {posargs}
    py{35,36,37}-!external-!nocover: mv {envtmpdir}/.coverage {toxinidir}/.coverage-reports/.coverage.{env:COVERAGE_PREFIX:}{envname}
    py{35,36,37}-external-!nocover: - mv {envtmpdir}/.coverage {toxinidir}/.coverage-reports/.coverage.{env:COVERAGE_PREFIX:}{envname}
    # Running with coverage reporting disabled
    py{35,36,37}-!external-nocover: pytest --basetemp {envtmpdir} {posargs}
    external-nocover: {envpython} {toxinidir}/tests/external_plugins.py {envtmpdir} {posargs}
deps =
    py{35,36,37}: -rrequirements/requirements.txt
    py{35,36,37}: -rrequirements/dev-requirements.txt
    py{35,36,37}: -rrequirements/plugin-requirements.txt

    # Only require coverage and pytest-cov when using it
    !nocover: -rrequirements/cov-requirements.txt
passenv =
    ARTIFACT_CACHE_SERVICE
    BST_FORCE_BACKEND
    BST_FORCE_SANDBOX
    GI_TYPELIB_PATH
    INTEGRATION_CACHE
    http_proxy
    HTTP_PROXY
    https_proxy
    HTTPS_PROXY
    no_proxy
    NO_PROXY
    REMOTE_EXECUTION_SERVICE
    SOURCE_CACHE_SERVICE
    SSL_CERT_FILE

#
# These keys are not inherited by any other sections
#
setenv =
    py{35,36,37}: COVERAGE_FILE = {envtmpdir}/.coverage
    py{35,36,37}: XDG_CACHE_HOME = {envtmpdir}/cache
    py{35,36,37}: XDG_CONFIG_HOME = {envtmpdir}/config
    py{35,36,37}: XDG_DATA_HOME = {envtmpdir}/share
    # This is required to get coverage for Cython
    py{35,36,37}-!nocover: BST_CYTHON_TRACE = 1

whitelist_externals =
    py{35,36,37}:
        mv
        mkdir

#
# Coverage reporting
#
[testenv:coverage]
# This is required by Cython in order to get coverage for cython files.
usedevelop = True

commands =
    coverage combine --rcfile={toxinidir}/.coveragerc {toxinidir}/.coverage-reports/
    coverage html --rcfile={toxinidir}/.coveragerc --directory={toxinidir}/.coverage-reports/
    coverage report --rcfile={toxinidir}/.coveragerc --show-missing
deps =
    -rrequirements/cov-requirements.txt
setenv =
    COVERAGE_FILE = {toxinidir}/.coverage-reports/.coverage

#
# Running linters
#
[testenv:lint]
commands_pre =
    # Build C extensions to allow Pylint to analyse them
    {envpython} setup.py build_ext --inplace

commands =
    pycodestyle
    pylint src/buildstream tests
deps =
    -rrequirements/requirements.txt
    -rrequirements/dev-requirements.txt
    -rrequirements/plugin-requirements.txt


#
# Building documentation
#
[testenv:docs]
commands =
    make -C doc
# sphinx_rtd_theme < 0.4.2 breaks search functionality for Sphinx >= 1.8
deps =
    sphinx >= 1.8.5
    sphinx-click
    sphinx_rtd_theme >= 0.4.2
    pytest
    -rrequirements/requirements.txt
    -rrequirements/plugin-requirements.txt
passenv =
    BST_FORCE_SESSION_REBUILD
    BST_SOURCE_CACHE
    HOME
    LANG
    LC_ALL
whitelist_externals =
    make

#
# (re-)Generating man pages
#
[testenv:man]
commands =
    python3 setup.py --command-packages=click_man.commands man_pages
deps =
    click-man >= 0.3.0
    -rrequirements/requirements.txt

#
# Usefull for running arbitrary scripts in a BuildStream virtual env
#
[testenv:venv]
commands = {posargs}
deps =
    -rrequirements/requirements.txt
    -rrequirements/dev-requirements.txt
    -rrequirements/plugin-requirements.txt
whitelist_externals = *


#
# Convenience environment for running individual tests from the
# battery of templated source tests.
#
# You should pass this the part of a test node's id after "::". For
# example, to run the test
# buildstream/testing/_sourcetests/fetch.py::test_fetch_cross_junction[git-inline]
# you would do tox -e sourcetests -- test_fetch_cross_junction[git-inline]
#
# This does rely on the fact that none of the tests in
# buildstream.testing have the same name.
#
[testenv:sourcetests]
commands = pytest --basetemp {envtmpdir} --ignore tests -k "{posargs}"
deps =
    -rrequirements/requirements.txt
    -rrequirements/dev-requirements.txt
    -rrequirements/plugin-requirements.txt

# When building using PEP518 and 517, we don't want default dependencies
# installed by the base environment.
[testenv:.package]
deps =
